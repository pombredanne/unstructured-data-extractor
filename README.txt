To get started, change into the extract_tags/ directory and run the following
command:

$ python extract.py gauss ../examples/example3-gauss.html

Also have a look at import_demo.py for an indication of how to use the
extractor as a module.
